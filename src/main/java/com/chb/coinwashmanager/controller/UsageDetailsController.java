package com.chb.coinwashmanager.controller;


import com.chb.coinwashmanager.entity.Machine;
import com.chb.coinwashmanager.entity.Member;
import com.chb.coinwashmanager.model.CommonResult;
import com.chb.coinwashmanager.model.ListResult;
import com.chb.coinwashmanager.model.UsageDetailsItem;
import com.chb.coinwashmanager.model.UsageDetailsRequest;
import com.chb.coinwashmanager.service.MachineService;
import com.chb.coinwashmanager.service.MemberService;
import com.chb.coinwashmanager.service.ResponseService;
import com.chb.coinwashmanager.service.UsageDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "이용내역")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-details")
public class UsageDetailsController {
    private final UsageDetailsService usageDetailsService;
    private final MemberService memberService;
    private final MachineService machineService;


    @ApiOperation(value = "이용내역 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailsRequest request) {
        Member member = memberService.getMemberData(request.getMemberId());
        Machine machine = machineService.getMachineData(request.getMachineId());

        usageDetailsService.setUsageDetails(member, machine, request.getDateUsage());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "이용내역 날짜 확인")
    @GetMapping("/search")
    public ListResult<UsageDetailsItem> getUsageDetails(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate dateStart,
            @RequestParam(value = "dateEnd")  @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
            ) {
        return ResponseService.getListResult(usageDetailsService.getUsageDetails(dateStart, dateEnd), true);
    }
}
