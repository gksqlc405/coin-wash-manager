package com.chb.coinwashmanager.controller;

import com.chb.coinwashmanager.model.*;
import com.chb.coinwashmanager.service.MemberService;
import com.chb.coinwashmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "회원 정보 관계")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "회원가입 정보 등록")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest joinRequest) {
        memberService.setMember(joinRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원정보 모두 가저오기")
    @GetMapping("/all")
    public ListResult<MemberItem> getMembers() {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }

    @ApiOperation(value = "회원정보 유효상태 확인")
    @GetMapping("/isEnable")
    public ListResult<MemberItem> getMembersIsEnable(Boolean isEnable) {
        return ResponseService.getListResult(memberService.getMembers(isEnable), true);
    }

    @ApiOperation(value = "회원정보 수정")
    @PutMapping("/member/{memberId}")
    public CommonResult putMember(@PathVariable long memberId, @RequestBody @Valid MemberJoinRequest joinRequest) {
        memberService.putMember(memberId, joinRequest);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "회원 전화번호 수정")
    @PutMapping("/memberPhone/{memberPhoneId}")
    public CommonResult putMemberPhone(@PathVariable long memberPhoneId, @RequestBody @Valid MemberPhoneUpdateRequest phoneUpdateRequest) {
        memberService.putMemberPhone(memberPhoneId, phoneUpdateRequest);
        return ResponseService.getSuccessResult();
    }


    @ApiOperation(value = "회원 정보 탈퇴")
    @DeleteMapping("/{id}")
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);
        return ResponseService.getSuccessResult();
    }
}
