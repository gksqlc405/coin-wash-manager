package com.chb.coinwashmanager.controller;

import com.chb.coinwashmanager.enums.MachineType;
import com.chb.coinwashmanager.model.*;
import com.chb.coinwashmanager.service.MachineService;
import com.chb.coinwashmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기계 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {
    private final MachineService machineService;

    @ApiOperation(value = "기계 정보 등록")
    @PostMapping("/new")
    public CommonResult setMachine(@RequestBody @Valid MachineRequest request) {
        machineService.setMachine(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기계정보 하나 가저오기")
    @GetMapping("/{id}")
    public SingleResult<MachineDetail> getMachine(@PathVariable long id) {
        return ResponseService.getSingleResult(machineService.getMachine(id));
    }

    @ApiOperation(value = "기계정보 가저오기")
    @GetMapping("/search")
    public ListResult<MachineItem> getMachines(@RequestParam(value = "machineType", required = false) MachineType machineType) {
        if (machineType == null) {
            return ResponseService.getListResult(machineService.getMachines(), true);
        } else {
            return ResponseService.getListResult(machineService.getMachines(machineType), true);
        }
    }

    @ApiOperation(value = "기계 이름 수정")
    @PutMapping("/machineName/{machineNameId}")
    public CommonResult putMachineName(@PathVariable  long machineNameId, @RequestBody @Valid MachineNameUpdateRequest nameUpdateRequest) {
        machineService.putMachineName(machineNameId, nameUpdateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기계 정보 수정")
    @PutMapping("/machine/{machineId}")
    public CommonResult putMachine(@PathVariable  long machineId, @RequestBody @Valid MachineUpdateRequest request) {
        machineService.putMachine(machineId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기계 정보 삭제")
    @DeleteMapping("/delete/{machineId}")
    public CommonResult delMachine(@PathVariable long machineId) {
        machineService.delMachine(machineId);
        return ResponseService.getSuccessResult();
    }


}
