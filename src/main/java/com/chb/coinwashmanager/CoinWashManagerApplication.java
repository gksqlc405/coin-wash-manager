package com.chb.coinwashmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoinWashManagerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoinWashManagerApplication.class, args);
	}

}
