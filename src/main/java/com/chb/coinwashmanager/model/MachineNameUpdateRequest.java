package com.chb.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MachineNameUpdateRequest {
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "기계 이름", required = true)
    private String machineName;
}
