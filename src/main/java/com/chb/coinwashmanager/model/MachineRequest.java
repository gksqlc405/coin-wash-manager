package com.chb.coinwashmanager.model;

import com.chb.coinwashmanager.enums.MachineType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MachineRequest {
    @NotNull
    @ApiModelProperty(notes = "기계 타입", required = true)
    @Enumerated(EnumType.STRING)
    private MachineType machineType;

    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "기계 이름", required = true)
    private String machineName;

    @NotNull
    @ApiModelProperty(notes = "구매일", required = true)
    private LocalDate datePurchase;

    @NotNull
    @ApiModelProperty(notes = "가격", required = true)
    private Double machinePrice;

}
