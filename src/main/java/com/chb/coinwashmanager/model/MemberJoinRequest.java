package com.chb.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class MemberJoinRequest {

    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "회원 이름", required = true)
    private String memberName;

    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "회원 전화번호", required = true)
    private String memberPhone;

    @NotNull
    @ApiModelProperty(notes = "회원 생년월일", required = true)
    private LocalDate birthday;

}
