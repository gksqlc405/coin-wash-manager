package com.chb.coinwashmanager.model;

import com.chb.coinwashmanager.entity.UsageDetails;
import com.chb.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailsItem {
    private Long usageDetailsId;

    private LocalDateTime dateUsage;

    private Long machineId;

    private String machineFullName;

    private Long memberId;

    private String memberName;

    private String memberPhone;

    private LocalDate memberBirthday;

    private Boolean memberIsEnable;

    private LocalDateTime memberDateJoin;

    private LocalDateTime memberDateWithdrawal;

    private UsageDetailsItem(UsageDetailsItemBuilder builder) {
        this.usageDetailsId = builder.usageDetailsId;
        this.dateUsage = builder.dateUsage;
        this.machineId = builder.machineId;
        this.machineFullName = builder.machineFullName;
        this.memberId = builder.memberId;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.memberBirthday = builder.memberBirthday;
        this.memberIsEnable = builder.memberIsEnable;
        this.memberDateJoin = builder.memberDateJoin;
        this.memberDateWithdrawal = builder.memberDateWithdrawal;
    }

    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailsItem> {
        @NotNull
        @ApiModelProperty(notes = "이용내역 시퀀스")
        private final Long usageDetailsId;
        @NotNull
        @ApiModelProperty(notes = "이용내역 시간")
        private final LocalDateTime dateUsage;
        @NotNull
        @ApiModelProperty(notes = "기계 시퀀스")
        private final Long machineId;
        @NotNull
        @ApiModelProperty(notes = "기계 종류와 이름")
        @Length(min = 1, max = 30)
        private final String machineFullName;
        @NotNull
        @ApiModelProperty(notes = "회원 시퀀스")
        private final Long memberId;
        @NotNull
        @ApiModelProperty(notes = "회원 이름")
        @Length(min = 1, max = 30)
        private final String memberName;
        @NotNull
        @ApiModelProperty(notes = "회원 전화번호")
        @Length(min = 1, max = 30)
        private final String memberPhone;
        @NotNull
        @ApiModelProperty(notes = "회원 생년월일")
        private final LocalDate memberBirthday;
        @NotNull
        @ApiModelProperty(notes = "회원 유효여부")
        private final Boolean memberIsEnable;
        @NotNull
        @ApiModelProperty(notes = "회원 가입일")
        private final LocalDateTime memberDateJoin;
        @NotNull
        @ApiModelProperty(notes = "회원 탈퇴")
        private final LocalDateTime memberDateWithdrawal;

        public UsageDetailsItemBuilder(UsageDetails usageDetails) {
            this.usageDetailsId = usageDetails.getId();
            this.dateUsage = usageDetails.getDateUsage();
            this.machineId = usageDetails.getMachine().getId();
            this.machineFullName = usageDetails.getMachine().getMachineType().getName() + " " + usageDetails.getMachine().getMachineName();
            this.memberId = usageDetails.getMember().getId();
            this.memberName = usageDetails.getMember().getMemberName();
            this.memberPhone = usageDetails.getMember().getMemberPhone();
            this.memberBirthday = usageDetails.getMember().getBirthday();
            this.memberIsEnable = usageDetails.getMember().getIsEnable();
            this.memberDateJoin = usageDetails.getMember().getDateJoin();
            this.memberDateWithdrawal = usageDetails.getMember().getDateWithdrawal();
        }

        @Override
        public UsageDetailsItem build() {
            return new UsageDetailsItem(this);
        }
    }
}
