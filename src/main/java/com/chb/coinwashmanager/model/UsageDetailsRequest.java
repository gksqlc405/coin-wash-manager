package com.chb.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class UsageDetailsRequest {
    @NotNull
    @ApiModelProperty(notes = "회원 시퀀스", required = true)
    private Long memberId;

    @NotNull
    @ApiModelProperty(notes = "기계 시퀀스", required = true)
    private Long machineId;

    @NotNull
    @ApiModelProperty(notes = "이용시간", required = true)
    private LocalDateTime dateUsage;
}
