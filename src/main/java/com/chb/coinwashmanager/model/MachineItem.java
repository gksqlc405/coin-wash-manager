package com.chb.coinwashmanager.model;

import com.chb.coinwashmanager.entity.Machine;
import com.chb.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineItem {
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "기계 시퀀스")
    private Long id;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "기계 종류와 이름")
    private String machineFullName;
    @NotNull
    @ApiModelProperty(notes = "구매일")
    private LocalDate datePurchase;

    private MachineItem(MachineItemBuilder builder) {
        this.id = builder.id;
        this.machineFullName = builder.machineFullName;
        this.datePurchase = builder.datePurchase;

    }

    public static class MachineItemBuilder implements CommonModelBuilder<MachineItem> {
        private final Long id;
        private final String machineFullName;
        private final LocalDate datePurchase;

        public MachineItemBuilder(Machine machine) {
            this.id = machine.getId();
            this.machineFullName = machine.getMachineType().getName() + " " + machine.getMachineName();
            this.datePurchase = machine.getDatePurchase();
        }


        @Override
        public MachineItem build() {
            return new MachineItem(this);
        }
    }
}
