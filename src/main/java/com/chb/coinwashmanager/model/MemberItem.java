package com.chb.coinwashmanager.model;

import com.chb.coinwashmanager.entity.Member;
import com.chb.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {

    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long id;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "회원 이름")
    private String memberName;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "회원 전화번호")
    private String memberPhone;
    @NotNull
    @ApiModelProperty(notes = "회원 생일")
    private LocalDate birthday;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "회원 유효여부")
    private String isEnable;
    @NotNull
    @ApiModelProperty(notes = "회원 가입일")
    private LocalDateTime dateJoin;
    @NotNull
    @ApiModelProperty(notes = "회원 탈퇴일")
    private LocalDateTime dateWithdrawal;

    private MemberItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.birthday = builder.birthday;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateWithdrawal = builder.dateWithdrawal;


    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {

        private final Long id;
        private final String memberName;
        private final String memberPhone;
        private final LocalDate birthday;
        private final String isEnable;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateWithdrawal;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.memberPhone = member.getMemberPhone();
            this.birthday = member.getBirthday();
            this.isEnable = member.getIsEnable() ? "예" : "아니오";
            this.dateJoin = member.getDateJoin();
            this.dateWithdrawal = member.getDateWithdrawal();
        }
        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
