package com.chb.coinwashmanager.model;

import com.chb.coinwashmanager.entity.Machine;
import com.chb.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MachineDetail {
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "기계 시퀀스")
    private Long id;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "기계 종류")
    private String machineTypeName;
    @NotNull
    @Length(min = 1, max = 20)
    @ApiModelProperty(notes = "기계 이름")
    private String machineName;
    @NotNull
    @ApiModelProperty(notes = "구매일")
    private LocalDate datePurchase;
    @NotNull
    @ApiModelProperty(notes = "가격")
    private Double machinePrice;

    private MachineDetail(MachineDetailBuilder builder) {
        this.id = builder.id;
        this.machineTypeName = builder.machineTypeName;
        this.machineName = builder.machineName;
        this.datePurchase = builder.datePurchase;
        this.machinePrice = builder.machinePrice;

    }

    public static class MachineDetailBuilder implements CommonModelBuilder<MachineDetail> {

        private final Long id;

        private final String machineTypeName;

        private final String machineName;

        private final LocalDate datePurchase;

        private final Double machinePrice;

        public MachineDetailBuilder(Machine machine) {
            this.id = machine.getId();
            this.machineTypeName = machine.getMachineType().getName();
            this.machineName = machine.getMachineName();
            this.datePurchase = machine.getDatePurchase();
            this.machinePrice = machine.getMachinePrice();


        }

        @Override
        public MachineDetail build() {
            return new MachineDetail(this);
        }
    }
}
