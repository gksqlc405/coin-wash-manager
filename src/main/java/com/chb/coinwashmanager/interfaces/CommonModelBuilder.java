package com.chb.coinwashmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
