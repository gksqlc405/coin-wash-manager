package com.chb.coinwashmanager.service;

import com.chb.coinwashmanager.entity.Machine;
import com.chb.coinwashmanager.entity.Member;
import com.chb.coinwashmanager.entity.UsageDetails;
import com.chb.coinwashmanager.model.ListResult;
import com.chb.coinwashmanager.model.UsageDetailsItem;
import com.chb.coinwashmanager.repository.UsageDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {
    private final UsageDetailsRepository usageDetailsRepository;


    public void setUsageDetails(Member member, Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member, dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }

    public ListResult<UsageDetailsItem> getUsageDetails(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(
                dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );

        List<UsageDetails> usageDetails =
                usageDetailsRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);
        List<UsageDetailsItem> result = new LinkedList<>();
        usageDetails.forEach(usageDetail -> {
            UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetail).build();
            result.add(addItem);
        });
        return ListConvertService.settingResult(result);

    }

}
