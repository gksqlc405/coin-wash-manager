package com.chb.coinwashmanager.service;

import com.chb.coinwashmanager.entity.Machine;
import com.chb.coinwashmanager.enums.MachineType;
import com.chb.coinwashmanager.exception.CMissingDataException;
import com.chb.coinwashmanager.model.*;
import com.chb.coinwashmanager.repository.MachineRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MachineService {
    private final MachineRepository machineRepository;

    public Machine getMachineData(long id) {
        return machineRepository.findById(id).orElseThrow(CMissingDataException::new);

    }

    public void setMachine(MachineRequest request) {
        Machine machine = new Machine.MachineBuilder(request).build();
        machineRepository.save(machine);
    }

    public MachineDetail getMachine(long id) {
        Machine machine = machineRepository.findById(id).orElseThrow(ConcurrentModificationException::new);
        return new MachineDetail.MachineDetailBuilder(machine).build();
    }

    public ListResult<MachineItem> getMachines() {
        List<Machine> machines = machineRepository.findAll();

        List<MachineItem> result = new LinkedList<>();

        machines.forEach(machine -> {
            MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
        }

        public ListResult<MachineItem> getMachines(MachineType machineType) {
        List<Machine> machines = machineRepository.findAllByMachineTypeOrderByIdDesc(machineType);

            List<MachineItem> result = new LinkedList<>();

            machines.forEach(machine -> {
                MachineItem addItem = new MachineItem.MachineItemBuilder(machine).build();
                result.add(addItem);
            });

            return ListConvertService.settingResult(result);
        }

        public void putMachineName(long id, MachineNameUpdateRequest updateRequest) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putDataName(updateRequest);
        machineRepository.save(machine);
        }

        public void putMachine(long id, MachineUpdateRequest request) {
        Machine machine = machineRepository.findById(id).orElseThrow(CMissingDataException::new);
        machine.putMachine(request);
        machineRepository.save(machine);
        }

        public void delMachine(long machineId) {
        machineRepository.deleteById(machineId);
        }
}
