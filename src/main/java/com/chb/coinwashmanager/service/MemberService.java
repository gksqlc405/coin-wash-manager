package com.chb.coinwashmanager.service;

import com.chb.coinwashmanager.entity.Member;
import com.chb.coinwashmanager.exception.CMissingDataException;
import com.chb.coinwashmanager.exception.CNoMemberDataException;
import com.chb.coinwashmanager.model.ListResult;
import com.chb.coinwashmanager.model.MemberItem;
import com.chb.coinwashmanager.model.MemberJoinRequest;
import com.chb.coinwashmanager.model.MemberPhoneUpdateRequest;
import com.chb.coinwashmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getMemberData(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        return member;
    }

    public void setMember(MemberJoinRequest joinRequest) {
        Member member = new Member.MemberBuilder(joinRequest).build();
        memberRepository.save(member);
    }

    public ListResult<MemberItem> getMembers() {
        List<Member> members = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<MemberItem> getMembers(boolean isEnable) {
        List<Member> members = memberRepository.findAllByIsEnableOrderByIdDesc(isEnable);

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putMemberWithdrawal(long id) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CNoMemberDataException();

        member.putWithdrawal();
        memberRepository.save(member);
    }

    public void putMember(long id, MemberJoinRequest request) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMember(request);

        memberRepository.save(member);
    }

    public void putMemberPhone(long id, MemberPhoneUpdateRequest phoneUpdateRequest) {
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);
        member.putMemberPhone(phoneUpdateRequest);
        memberRepository.save(member);
    }




}

