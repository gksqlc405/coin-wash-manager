package com.chb.coinwashmanager.repository;

import com.chb.coinwashmanager.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {
    List<Member> findAllByIsEnableOrderByIdDesc(Boolean isEnable);
}
