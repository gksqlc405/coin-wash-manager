package com.chb.coinwashmanager.repository;

import com.chb.coinwashmanager.entity.Machine;
import com.chb.coinwashmanager.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByMachineTypeOrderByIdDesc(MachineType machineType);
}
